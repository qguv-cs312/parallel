/*
	Parallel Programming Project
	Common functions
	Quint Guvernator
	CSCI 312, 2015
*/

// modules
#include <stdlib.h>   // free()
#include <stdio.h>    // printf()
#include <sys/time.h> // gettimeofday() for timing
#include <time.h>     // clock() for random generation
#include <math.h>     // sqrt()
#include "common.h"

// Solve a randomly generated matrix multiplication problem using Gaussian
// elimination, spitting out statistics about the solution's speed and
// accuracy.
void time_random_solve(int length) {

	// malloc and randomize vector and matrix
	Problem *p = (Problem *) malloc(sizeof(Problem));
	p->a = generate_matrix(length);
	p->b = generate_vector(length);
	p->n = length;

	// malloc and zero a solution vector for later
	p->x = (double *) malloc(sizeof(double) * length);
	for (int i=0; i < length; i++) { p->x[i] = (double) 0; };

	show(p, "Generated");

	// begin timing
	struct timeval tv;
	gettimeofday(&tv, NULL);
	int start_s = tv.tv_sec;
	int start_us = tv.tv_usec;

	eliminate(p);

	// end timing
	gettimeofday(&tv, NULL);
	int end_s = tv.tv_sec;
	int end_us = tv.tv_usec;

	show(p, "After Elimination");

	// give a summary
	printf("=== Summary ===\n");
	printf("Matrix:  %i by %i doubles\n", length, length);
	printf("Time:    %.4f seconds\n",
			(double)((end_s - start_s) + (end_us - start_us) / 1e6));
	printf("L2 norm: %e\n\n\n", calc_L2_norm(p));

	free(p->a);
	free(p->b);
	free(p->x);
	free(p);
}

void show(Problem *p, const char *header) {

	// only show if it's sufficiently small
	if (p->n > 20) { return; }

	printf("\n=== %s ===\n", header);
	printf("matrix a:\n");
	for (int i=0; i < p->n; i++) {
		printf("row %2i: ", i + 1);
		for (int j=0; j < p->n; j++) {
			printf("% .4f ", *matrix(p, i, j));
		}
		printf("\n");
	}

	printf("\nvector b:\n");
	for (int i=0; i < p->n; i++) { printf("% .4f ", p->b[i]); }

	printf("\n\nvector x:\n");
	for (int i=0; i < p->n; i++) { printf("% .4f ", p->x[i]); }
	printf("\n\n");
}

double *generate_matrix(int length) { return generate_vector(length * length); }
double *generate_vector(int length) {

	// local random number generation
	double rx;
	struct drand48_data rand_buffer;
	srand48_r(clock(), &rand_buffer);

	// seed the whole thing with random doubles
	double *v = malloc(sizeof(double) * length);
	for (int i=0; i < length; i++) {
		drand48_r(&rand_buffer, &rx);
		v[i] = rx;
	}

	return v;
}

double calc_L2_norm(Problem *p) {
	double row_sum;
	double residual[p->n];

	for (int i=0; i < p->n; i++) {
		row_sum = (double) 0;
		for (int j=0; j < p->n; j++) {
			row_sum += *matrix(p, i, j) * p->x[j];
		}
		residual[i] = row_sum;
	}

	for (int i=0; i < p->n; i++) {
		residual[i] -= p->b[i];
	}

	double sum_squares = (double) 0;
	for (int i=0; i < p->n; i++) {
		sum_squares += residual[i] * residual[i];
	}

	return sqrt(sum_squares);
}

void swap_rows(Problem *p, int row_to, int row_from) {
	if (row_to == row_from) return;

	double swap;
	double *c1, *c2;

	// swap matrix rows
	for (int col = 0; col < p->n; col++) {
		c1 = matrix(p, row_to, col);
		c2 = matrix(p, row_from, col);
		swap = *c1, *c1 = *c2, *c2 = swap;
	}

	// swap corresponding vector elements
	swap = p->b[row_to], p->b[row_to] = p->b[row_from], p->b[row_from] = swap;
}

double *matrix(Problem *p, int row, int col) { return p->a + (row * p->n + col); }
