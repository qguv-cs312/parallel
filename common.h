/*
	Parallel Programming Project
	Common declarations
	Quint Guvernator
	CSCI 312, 2015
*/

// constants
static int ERR_CLI_OPTIONS = 2;

// structs
typedef struct {
	double *a, *b, *x;
	int n;
} Problem;

typedef struct {
	Problem *p;
	int diag;
	int min_row;
	int max_row;
} ParallelArgs;

// prototypes
void   time_random_solve(int length);
void   swap_rows(Problem *p, int row_to, int row_from);
void   eliminate(Problem *p);
void   *parallel_eliminate(void *args);
double calc_L2_norm(Problem *p);
double *generate_matrix(int length);
double *generate_vector(int length);
void   show(Problem *p, const char *header);
double *matrix(Problem *p, int row, int col);
