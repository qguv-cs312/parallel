/*
	Parallel Programming Project
	Sequential implementation
	Quint Guvernator
	CSCI 312, 2015
*/

// modules
#include <stdlib.h> // atoi()
#include <stdio.h>  // printf()
#include <math.h>   // fabs()
#include "common.h"

// Perform gaussian elimination on a given problem, mutating the matrix,
// vector, and the solution vector. Inspired by the algorithm at:
// http://rosettacode.org/wiki/Gaussian_elimination
void eliminate(Problem *p) {
	#define A(y, x) (*matrix(p, y, x))
	int col, max_row;
	double max, tmp;

	// create an upper triangular matrix
	for (int diag = 0; diag < p->n; diag++) {
		max_row = diag, max = A(diag, diag);

		// find the maximum row
		for (int row = diag + 1; row < p->n; row++) {
			tmp = fabs(A(row, diag));
			if (tmp > max) {
				max_row = row, max = tmp;
			}
		}

		// swap the diagonal with the row maximum
		swap_rows(p, diag, max_row);

		// gaussean elimination
		for (int row = diag + 1; row < p->n; row++) {
			double c = A(row, diag) / A(diag, diag);
			for (int col = diag+1; col < p->n; col++) {
				A(row, col) -= c * A(diag, col);
			}
			A(row, diag) = 0;
			p->b[row] -= c * p->b[diag];
		}
	}

	// solve for x vector
	for (int row = p->n - 1; row >= 0; row--) {
		tmp = p->b[row];
		for (int j = p->n - 1; j > row; j--) {
			tmp -= p->x[j] * A(row, j);
		}
		p->x[row] = tmp / A(row, row);
	}
	#undef A
}

int main(int argc, char* argv[]) {
	if (argc < 2) {
		printf("Usage: %s <matsize>\n", argv[0]); 
		return ERR_CLI_OPTIONS;
	}

	time_random_solve(atoi(argv[1]));
	return 0;
}
