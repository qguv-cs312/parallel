# Parallel Programming Project
# GNU-style makefile
# Quint Guvernator
# CSCI 312, 2015

CC=gcc
CFLAGS=-I. -std=gnu99 -lm -fopenmp -lpthread -g
DEPS = common.h
OBJ = common.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

all: openmp pthread

openmp: $(OBJ) openmp.o
	gcc -o $@ $^ $(CFLAGS)

pthread: $(OBJ) pthread.o
	gcc -o $@ $^ $(CFLAGS)

sequential: $(OBJ) sequential.o
	gcc -o $@ $^ $(CFLAGS)

omp-view: openmp
	OMP_NUM_THREADS=1 ./openmp 10
	OMP_NUM_THREADS=4 ./openmp 10

omp-test: openmp
	OMP_NUM_THREADS=1 ./openmp 750
	OMP_NUM_THREADS=4 ./openmp 750

omp-measure: openmp
	OMP_NUM_THREADS=1 ./openmp 3000
	OMP_NUM_THREADS=2 ./openmp 3000
	OMP_NUM_THREADS=3 ./openmp 3000
	OMP_NUM_THREADS=4 ./openmp 3000

pth-view: pthread
	./pthread 10 1
	./pthread 10 4

pth-test: pthread
	./pthread 750 1
	./pthread 750 4

pth-measure: pthread
	./pthread 3000 1
	./pthread 3000 2
	./pthread 3000 3
	./pthread 3000 4

seq-view: sequential
	./sequential 10

seq-test: sequential
	./sequential 750

seq-measure: sequential
	./sequential 3000

clean:
	rm -f pthread openmp sequential *.o
