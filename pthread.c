/*
	Parallel Programming Project
	Pthread implementation
	Quint Guvernator
	CSCI 312, 2015
*/

// modules
#include <stdlib.h> // atoi()
#include <stdio.h>  // printf()
#include <math.h>   // fabs()
#include <pthread.h>
#include "common.h"

// globals
int NUM_THREADS;
pthread_attr_t THREAD_ATTRIBUTES;

// Perform gaussian elimination on a given problem, mutating the matrix,
// vector, and the solution vector. Inspired by the algorithm at:
// http://rosettacode.org/wiki/Gaussian_elimination
void eliminate(Problem *p) {
	#define A(y, x) (*matrix(p, y, x))
	int col, max_row;
	double max, tmp;

	// create an upper triangular matrix
	for (int diag = 0; diag < p->n; diag++) {
		max_row = diag, max = A(diag, diag);

		// find the maximum row
		for (int row = diag + 1; row < p->n; row++) {
			tmp = fabs(A(row, diag));
			if (tmp > max) {
				max_row = row, max = tmp;
			}
		}

		// swap the diagonal with the row maximum
		swap_rows(p, diag, max_row);

		// gaussean elimination with pthreads
		pthread_t p_threads[NUM_THREADS];
		ParallelArgs *p_thread_args[NUM_THREADS];
		for (int i=0; i < NUM_THREADS; i++) {
			ParallelArgs *pa = (ParallelArgs *) malloc(sizeof(ParallelArgs));
			pa->p = p;
			pa->diag = diag;
			pa->min_row = diag + 1 + i;
			pa->max_row = p->n - 1;

			p_thread_args[i] = pa;
			pthread_create(&p_threads[i], &THREAD_ATTRIBUTES, parallel_eliminate, (void *) pa);
		}

		// collect finished threads
		for (int i=0; i < NUM_THREADS; i++) {
			pthread_join(p_threads[i], NULL);
			free(p_thread_args[i]);
		}
	}

	// solve for x vector
	for (int row = p->n - 1; row >= 0; row--) {
		tmp = p->b[row];
		for (int j = p->n - 1; j > row; j--) {
			tmp -= p->x[j] * A(row, j);
		}
		p->x[row] = tmp / A(row, row);
	}
	#undef A
}

void *parallel_eliminate(void *args) {
	ParallelArgs *pa = (ParallelArgs *) args;
	int diag = pa->diag;
	Problem *p = pa->p;
	#define A(y, x) (*matrix(p, y, x))

	double tmp;

	for (int row = pa->min_row; row <= pa->max_row; row += NUM_THREADS) {
		tmp = A(row, diag) / A(diag, diag);
		for (int col = diag+1; col < p->n; col++) {
			A(row, col) -= tmp * A(diag, col);
		}
		A(row, diag) = 0;
		p->b[row] -= tmp * p->b[diag];
	}
	#undef A
}

int main(int argc, char* argv[]) {
	if (argc < 3) {
		printf("Usage: %s <matsize> <numthreads>\n", argv[0]); 
		return ERR_CLI_OPTIONS;
	}

	NUM_THREADS = atoi(argv[2]);
	pthread_attr_init(&THREAD_ATTRIBUTES); //TODO good enough?
	time_random_solve(atoi(argv[1]));
	return 0;
}
